public class OperatorMinus extends Operator {

    public OperatorMinus() {
        super("-", true, 2, 10);
    }

    @Override
    public double math (double [] array) {
        return array[0] - array[1];
    }
}