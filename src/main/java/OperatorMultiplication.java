public class OperatorMultiplication extends Operator {

    public OperatorMultiplication() {
        super("*", true, 2, 20);
    }

    @Override
    public double math (double [] array) {
        return array[0] * array[1];
    }
}