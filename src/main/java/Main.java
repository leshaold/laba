import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static Scanner in = new Scanner(System.in);
    public static String inputString () {
        String input = "";
        System.out.print("Введите выражение: ");
        input = in.nextLine();
        return input;
    }

    public static void main (String [] args) {
        OperatorDivision division = new OperatorDivision();
        OperatorPlus plus = new OperatorPlus();
        OperatorMinus minus = new OperatorMinus();
        OperatorMultiplication multiplication = new OperatorMultiplication();
        String input = "";
        Double result;
        ArrayList<Operator> operators = new ArrayList<>();
        operators.add(division);
        operators.add(plus);
        operators.add(minus);
        operators.add(multiplication);

        Calculator calculator = new Calculator(operators);
        do {
            input = inputString();
            if (input.isEmpty()) break;
            try {
                result = calculator.run(input);
                System.out.println(result);
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }
        while (true);
    }
}
