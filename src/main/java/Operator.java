public abstract class Operator {
    public String name;
    public Boolean inExpr;
    public int argCount;
    public int priority;

    public abstract double math (double [] array);

    public Operator(String name, Boolean inExpr, int argCount, int priority) {
        this.name = name;
        this.inExpr = inExpr;
        this.argCount = argCount;
        this.priority = priority;
    }
}