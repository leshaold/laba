public class Token {
    public int position;
    public String data;
    public type tokenType;

    public Token(int position, String data, type tokenType) {
        this.position = position;
        this.data = data;
        this.tokenType = tokenType;
    }
}

enum type {
    DIGIT,
    OPERATOR,
    OPEN_BRACKET,
    CLOSE_BRACKET
}