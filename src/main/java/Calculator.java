import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    private ArrayList<Operator> operators;
    private static Pattern regExpr = Pattern.compile("\\d+([.]\\d+)?");

    public Calculator(ArrayList<Operator> operators) {
        this.operators = operators;
    }

    public double run (String input) {
        LinkedList<Token> tokens = tokenize(input);
        tokens = preprocessing(tokens);
        tokens = postfix(tokens);
        return calc(tokens);

    }

    public LinkedList<Token> tokenize (String expression) {
        LinkedList<Token> tokens = new LinkedList<>();
        char [] tempChar = expression.toCharArray();
        for(int pos = 0; pos < tempChar.length; )
        {
            if (tempChar[pos] == ' ' || tempChar[pos] == '\t') {
                pos++;
            }
            else
            if (tempChar[pos] == ')') {
                Token token = new Token(pos, ")", type.CLOSE_BRACKET);
                tokens.add(token);
                pos++;
            }
            else
            if (tempChar[pos] == '(') {
                Token token = new Token(pos, "(", type.OPEN_BRACKET);
                tokens.add(token);
                pos++;
            }
            else
            {
                Boolean found = false;
                for (Operator o : operators) {
                    if (o.inExpr && expression.startsWith(o.name, pos))
                    {
                       found = true;
                       Token token = new Token(pos, o.name, type.OPERATOR);
                       tokens.add(token);
                       pos += o.name.length();
                    }
                }

                if (!found) {
                    Matcher m = regExpr.matcher(expression);
                    if (m.find(pos) && m.start() == pos)
                    {
                        Token token = new Token(pos, m.group(), type.DIGIT);
                        tokens.add(token);
                        pos += token.data.length();
                    }
                    else
                        throw new RuntimeException("Lexer error at " + pos);
                }
            }
        }
        return tokens;
    }

    public LinkedList<Token> preprocessing (LinkedList<Token> tokens ) {
            Token prev = null;
            LinkedList<Token> output = new LinkedList<>();
            for (Token T : tokens)
            {
                if (T.tokenType == type.OPERATOR && T.data == "-")
                {
                    if (prev == null || (prev.tokenType != type.DIGIT && prev.tokenType != type.CLOSE_BRACKET))
                    {
                        T.data = "-1";
                    }
                }
                else if (T.tokenType == type.OPERATOR && T.data == "+")
                {
                    if (prev == null || (prev.tokenType != type.DIGIT && prev.tokenType != type.CLOSE_BRACKET))
                    {
                        T.data = "+1";
                    }
                }
                else if (T.tokenType == type.OPEN_BRACKET && (prev.tokenType == type.CLOSE_BRACKET || prev.tokenType == type.DIGIT))
                {
                    Token temp = new Token(T.position, "*", type.OPERATOR);
                    output.add(temp);
                }
                output.add(T);
                prev = T;
            }

    }

    public LinkedList<Token> postfix (LinkedList<Token> tokens ) {
        return null;
    }

    public double calc (LinkedList<Token> tokens ) {
        Stack<Double> arg = new Stack<>();
        double [] mas = new double[2];
        for (Token t : tokens)
        {
            if (t.tokenType == type.DIGIT)
            {
                Double temp = Double.parseDouble(t.data);
                arg.push(temp);
            }
            else if (t.tokenType == type.OPERATOR)
            {
                Operator j = null;
                for (Operator i : operators)
                {
                    if (i.name == t.data)
                    {
                        j = i;
                        break;
                    }
                }
                if (j == null)
                    throw new RuntimeException("неизвестный оператор");
                for (int i = j.argCount-1; i>=0; i--)
                {
                    mas[i] = arg.pop();
                }
                arg.push(j.math(mas));
            }
        }
        if(arg.size() == 1) return arg.pop();
        else throw new RuntimeException("Something went wrong");
    }

}
